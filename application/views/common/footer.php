<div class="container">
  <div class="row">
    <footer class="col-md-12">
      <hr />
      <p>&copy; Sébastien Adam 2016</p>
    </footer>
  </div>
</div>
<script src="<?= base_url("js/jquery-3.0.0.min.js") ?>"></script>
<script src="<?= base_url("js/bootstrap.min.js") ?>"></script>
<?php
if (isset($script)) {
  echo $script;
}
?>
</body>
</html>
